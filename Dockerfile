FROM registry.gitlab.com/competitions4/sciroc/dockers/sciroc

LABEL maintainer="Rui Bettencourt <rui.bett.96@gmail.com>"

ARG REPO_WS=/ws
RUN mkdir -p $REPO_WS/src
WORKDIR /home/user/$REPO_WS

# Copy everything inside the docker to later be compiled in
COPY ./ws /home/user/ws

# I want my docker image to have git installed
RUN apt install -y git

# Build and source  my-ros-pkg packages 
RUN bash -c "source /opt/pal/ferrum/setup.bash \
    && catkin build \
    && echo 'source /opt/pal/ferrum/setup.bash' >> ~/.bashrc"

ENTRYPOINT ["bash"]
